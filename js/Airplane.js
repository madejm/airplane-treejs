
function Airplane () {

    var airplaneMaterial = new THREE.MeshPhongMaterial({color: 0xFFFFFF});

    var airplaneG = new THREE.CubeGeometry(180, 80, 80, 0, 0, 0);
    var airplaneM = new THREE.Mesh(airplaneG, airplaneMaterial);

    // TAIL

    var tailG = new THREE.ConeGeometry(56, 200, 4, false);
    var tailM = new THREE.Mesh(tailG, airplaneMaterial);
    tailM.rotation.z = Math.PI / 2;
    tailM.rotation.x = Math.PI / 4;
    tailM.position.x = -190;

    var tailVerticalG = new THREE.CubeGeometry(30, 70, 4, 0, 0, 0);
    var tailVerticalM = new THREE.Mesh(tailVerticalG, airplaneMaterial);
    tailVerticalM.position.x = -260;
    tailVerticalM.position.y = 35;
    tailVerticalM.rotation.z = Math.PI / 16;

    var tailHorizontalG = new THREE.CubeGeometry(40, 4, 140, 0, 0, 0);
    var tailHorizontalM = new THREE.Mesh(tailHorizontalG, airplaneMaterial);
    tailHorizontalM.position.x = -260;
    tailHorizontalM.position.y = 0;

    // WINGS

    var lowWingG = new THREE.CubeGeometry(120, 10, 440, 0, 0, 0);
    var lowWingM = new THREE.Mesh(lowWingG, airplaneMaterial);
    lowWingM.position.x = 30;
    lowWingM.position.y = -40;
    var highWingG = new THREE.CubeGeometry(120, 10, 500, 0, 0, 0);
    var highWingM = new THREE.Mesh(highWingG, airplaneMaterial);
    highWingM.position.x = 60;
    highWingM.position.y = 80;

    var wingConnectorG = new THREE.CubeGeometry(20, 130, 4, 0, 0, 0);
    var wingConnectorM1 = new THREE.Mesh(wingConnectorG, airplaneMaterial);
    wingConnectorM1.position.x = 80;
    wingConnectorM1.position.y = 20;
    wingConnectorM1.position.z = 220;
    wingConnectorM1.rotation.z = -Math.PI/12;
    wingConnectorM1.rotation.x = Math.PI/12;
    var wingConnectorM2 = new THREE.Mesh(wingConnectorG, airplaneMaterial);
    wingConnectorM2.position.x = 10;
    wingConnectorM2.position.y = 20;
    wingConnectorM2.position.z = 220;
    wingConnectorM2.rotation.z = -Math.PI/12;
    wingConnectorM2.rotation.x = Math.PI/12;
    var wingConnectorM3 = new THREE.Mesh(wingConnectorG, airplaneMaterial);
    wingConnectorM3.position.x = 80;
    wingConnectorM3.position.y = 20;
    wingConnectorM3.position.z = -220;
    wingConnectorM3.rotation.z = -Math.PI/12;
    wingConnectorM3.rotation.x = -Math.PI/12;
    var wingConnectorM4 = new THREE.Mesh(wingConnectorG, airplaneMaterial);
    wingConnectorM4.position.x = 10;
    wingConnectorM4.position.y = 20;
    wingConnectorM4.position.z = -220;
    wingConnectorM4.rotation.z = -Math.PI/12;
    wingConnectorM4.rotation.x = -Math.PI/12;

    // WHEELS

    var wheelMaterial = new THREE.MeshPhongMaterial({color: 0x111111});
    var wheelG = new THREE.CylinderGeometry(25, 25, 8, 20);
    var leftWheelM = new THREE.Mesh(wheelG, wheelMaterial);
    leftWheelM.position.x = 30;
    leftWheelM.position.y = -70;
    leftWheelM.position.z = -40;
    leftWheelM.rotation.x = Math.PI/2;
    var rightWheelM = new THREE.Mesh(wheelG, wheelMaterial);
    rightWheelM.position.x = 30;
    rightWheelM.position.y = -70;
    rightWheelM.position.z = 40;
    rightWheelM.rotation.x = Math.PI/2;

    var wheelPoleMaterial = new THREE.MeshPhongMaterial({color: 0x444444});
    var wheelPoleG = new THREE.CylinderGeometry(5, 5, 78, 20);
    var wheelPoleM = new THREE.Mesh(wheelPoleG, wheelPoleMaterial);
    wheelPoleM.position.x = 30;
    wheelPoleM.position.y = -70;
    wheelPoleM.rotation.x = Math.PI/2;

    // TURBINE

    var turbineMaterial = new THREE.MeshPhongMaterial({color: 0xff2222});
    var turbineG = new THREE.CubeGeometry(4, 20, 140, 0, 0, 0);
    var turbine1M = new THREE.Mesh(turbineG, turbineMaterial);
    turbine1M.rotation.x = Math.PI/4;
    var turbine2M = new THREE.Mesh(turbineG, turbineMaterial);
    turbine2M.rotation.x = -Math.PI/4;

    this.turbine = new THREE.Group();
    this.turbine.add( turbine1M );
    this.turbine.add( turbine2M );
    this.turbine.position.x = 110;

    // ADD

    this.object = new THREE.Group();
    this.object.add( airplaneM );
    this.object.add( tailM );
    this.object.add( tailVerticalM );
    this.object.add( tailHorizontalM );
    this.object.add( lowWingM );
    this.object.add( highWingM );
    this.object.add( wingConnectorM1 );
    this.object.add( wingConnectorM2 );
    this.object.add( wingConnectorM3 );
    this.object.add( wingConnectorM4 );
    this.object.add( leftWheelM );
    this.object.add( rightWheelM );
    this.object.add( wheelPoleM );
    this.object.add( this.turbine );
};